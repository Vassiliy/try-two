window.Application = () ->
  aD =
    servers:
      web:
        title: 'Web-accessible Heroku deployment'
        url: 'http://try-catch-vassiliy-pimkin.herokuapp.com/'
      local:
        title: 'Your locally assembled deployment'
        url: 'http://localhost:5000/'
    verbs:
      create: 'POST'
      update: 'PUT'
      destroy: 'DELETE'
    toggle_data:
      default:
        expand:
          word: ' Expand'
          class: 'btn-primary'
          glyph: 'chevron-down'
        collapse:
          word: ' Collapse'
          class: 'btn-info'
          glyph: 'chevron-up'
        loading:
          word: ' Loading...'
          class: 'btn-warning'
          glyph: 'time'
      topics:
        parent: '#topics-container'
        action: 'topics'
        expand:
          class: 'label-primary'
        collapse:
          class: 'label-info'
      users:
        parent: '#users-container'
        action: 'users'
        expand:
          class: 'label-primary'
        collapse:
          class: 'label-info'
      user:
        parent: '[data-form]'
        only: '[data-form]'
      user_topics:
        parent: '[data-form]'
        action: 'topics'
        skip_new: true
      user_posts:
        parent: '[data-form]'
        action: 'posts'
        skip_new: true
      topic:
        parent: '.panel'
        only: '.topics-list'
        action: 'posts'

  aM =
    csonify: (obj, indent) ->
      indent = if indent then indent + 1 else 1
      prefix = Array(indent).join '  '
      return ' "' + obj + '"' if typeof obj is 'string'
      return ' ' + obj if typeof obj isnt 'object'
      return ' [' + (aM.csonify(value) for value in obj).join(',') +
      ' ]' if Array.isArray obj
      return ('\n' + prefix + '"' + key + '":' +
      aM.csonify(value, indent) for key, value of obj).join ''

    data_selector: (key, names) ->
      selector = []
      for name in names
        selector.push "[data-#{key}*='#{name}']"
      selector.join ', '

    fields_selector: (controller) ->
      selector = []
      for tag in ['input', 'textarea']
        selector.push "#{tag}[data-type='#{controller}']"
      selector.join ', '

    security_status: (bool) ->
      if bool then 'secure' else 'breached'

    security_message: (item, status) ->
      "#{item} is #{status}"

    load_buffered: (jquery_element, append = false) ->
      $('#action-buffer').loadTemplate $('#template')
      if append
        jquery_element.append $('#content-buffer').html()
      else
        jquery_element.html $('#content-buffer').html()
      $('#content-buffer').empty()

    init_popup: (name, data, list_data) ->
      $('#popup-container')
      .loadTemplate $('#' + name + '-popup-window'), data
      $('#content-buffer')
      .loadTemplate $('#' + name + '-popup-list-item'), list_data
      aM.load_buffered $('#' + name + '-popup-list')

    open_popup: (name) ->
      $('#smoke-screen, #' + name + '-popup').removeClass 'hidden'

    close_popup: () ->
      $('#smoke-screen, #popup-container .popup').addClass 'hidden'
        
    init_server_popup: () ->
      servers_list = []
      for key, setting of aD.servers
        item =
          key: key
          title: setting.title
          url: setting.url
        servers_list.push item
      aM.init_popup 'server', {}, servers_list

    set_server: (location) ->
      local = location.match(/file/)?
      if local
        server = localStorage.getItem 'server'
        if server?
          aD.server = server
        else
          aM.choose_server()
      else
        aD.server = aD.servers.web.url
        aD.web_deployed = true

    choose_server: () ->
      server = if aD.server? then aD.server else 'none'
      popup = $('#server-popup')
      popup.find("[data-keep='server']").text server
      popup.find('.list-group-item.active').removeClass 'active'
      popup.find("[alt='#{server}']").addClass 'active'
      aM.open_popup 'server'

    set_stored_server: () ->
      key = $(@).attr 'value'
      aM.render_set_server aD.servers[key].url

    set_server_manually: () ->
      aM.render_set_server $('#server-popup input').val()

    render_set_server: (address) ->
      aM.close_popup()
      server_changed = aD.server isnt address
      aD.server = address
      localStorage.setItem 'server', address
      aM.logout() if server_changed

    flash_alert: (xhr) ->
      text = xhr.responseText
      if text is ''
        switch xhr.status
          when 404
            text = "Requested URL not found on server '#{aD.server}'."
          when 0
            text = 'Failed to load resource. Could not connect to the server.'
          else
            text = 'Unknown error.'
      params =
        code: xhr.status
        text: text
      $('#flash').loadTemplate $('#error-flash'), params,
        append: true

    render_login_info: () ->
      cog = if aD.web_deployed? then 'hidden' else ''
      if aD.username?
        role = if aD.admin then 'admin' else 'user'
        css = if aD.admin then 'btn-success' else 'btn-info'
        $('#login-info-container').loadTemplate $('#logout'),
          username: aD.username
          role: role
          class: css
          cog: cog
      else
        $('#login-info-container').loadTemplate $('#login'),
          cog: cog

    welcome: () ->
      aM.init_toggle_buttons()
      $('#work-space .toggle-collapse').click()

    logout: () ->
      delete aD.username
      delete aD.password
      delete aD.admin
      delete aD.user_id
      aM.render_login_info()
      aM.welcome()

    data_check_username: () ->
      aM.check_username $(@).val()

    check_username: (username) ->
      $.getJSON "#{aD.server}users.json",
        username: username
      .done (data) ->
        if data.length is 0
          $('nav [data-submit="users:update"]').addClass 'disabled'
          $('nav [data-form]').removeAttr 'value'
        else
          aD.username = username
          aD.admin = data[0].admin
          aD.user_id = data[0].id
          $('nav [data-submit="users:update"]').removeClass 'disabled'
          $('nav [data-form]').attr 'value', data[0].id

    enable_update: () ->
      form = $(@).parents('[data-form]')[0]
      collection_container = $(@).parents('[data-collection]')[0]
      type = $(collection_container).data 'collection'
      valid = true
      selector = aM.fields_selector type
      $(form).find(selector).each ->
        valid = false if $(@).val() is ''
      submit = aM.data_selector 'submit', ['update', 'create']
      button = $(form).find(submit)[0]
      if valid
        $(button).removeClass 'disabled'
      else
        $(button).addClass 'disabled'

    render_done_state: (model, verb, button, data) ->
      collection_container = $(button).parents "[data-collection*='#{model}']"
      form = $(button).parents('[data-form]')[0]
      switch verb
        when 'PUT'
          $(form).find('[data-submit*="update"]').addClass 'disabled'
          $('#topics-container .toggle-collapse').click() if model is 'user'
        when 'DELETE'
          $(form).remove()
          $('#topics-container .toggle-collapse').click() if model is 'user'
        else
          form.remove()
          aM.render_collection_item model, data
          aM.add_create_form model
          aM.load_buffered collection_container, true
          aM.init_toggle_buttons()

    common_template_data: (server_data) ->
      unless server_data.user?
        server_data.user =
          username: aD.username
          admin: aD.admin
      server_data.edited = if server_data.created_at is server_data.updated_at
      then 'hidden' else ''
      server_data.role = if server_data.user.admin
      then 'label label-success' else 'label label-info'
      server_data

    user_template_data: (server_data) ->
      user = server_data
      if user.admin
        user.role = 'admin'
        user.class = 'alert alert-success'
      else
        user.role = 'user'
        user.class = 'alert alert-info'
      for association in ['topics', 'posts']
        if user["#{association}_count"] is 0
          user["#{association}_button"] = 'hidden'
          user["#{association}_empty"] = ''
        else
          user["#{association}_button"] = ''
          user["#{association}_empty"] = 'hidden'
      user

    topic_template_data: (server_data) ->
      aM.common_template_data server_data

    post_template_data: (server_data) ->
      aM.common_template_data server_data

    render_collection_item: (item, server_data) ->
      template = item
      template_data = aM["#{item}_template_data"] server_data
      if aD.user_id?
        template = "#{item}-editable" if aD.admin
        template = "#{item}-editable" if aD.user_id is server_data.user_id
        if item is 'user'
          template = "#{item}-editable" if aD.user_id is server_data.id
      $('#content-buffer').loadTemplate $('#' + template), template_data,
        append: true

    add_create_form: (item) ->
      unless $("#new-#{item}").length is 0
        $('#content-buffer').loadTemplate $("#new-#{item}"), {},
          append: true

    load_collection: (items, jquery_container, params = {}, skip_new) ->
      item = items.slice 0, -1
      $.getJSON "#{aD.server}#{items}.json", params
      .done (data) ->
        jquery_container.loadTemplate $('#' + items)
        for item_data in data
          aM.render_collection_item item, item_data
        unless skip_new
          if aD.user_id?
            aM.add_create_form item
        aM.load_buffered jquery_container.find('.' + items + '-list')
      .fail (xhr, s) ->
        aM.flash_alert xhr

    get_toggle_info: (element) ->
      toggle_info = {}
      toggle_info.type = $(element).data 'toggle-button'
      toggle_info.state = if $(element).hasClass 'toggle-collapse'
      then 'collapse' else 'expand'
      toggle_info

    toggle_settings: (toggle_info) ->
      type = toggle_info.type
      state = toggle_info.state
      params = {}
      for key in ['word', 'class', 'glyph']
        if aD.toggle_data[type]?
          if aD.toggle_data[type][state]?
            if aD.toggle_data[type][state][key]?
              params[key] = aD.toggle_data[type][state][key]
        unless params[key]?
          params[key] = aD.toggle_data.default[state][key]
      params

    clear_toggle_settings: (jquery_element, settings) ->
      jquery_element.removeClass settings.class
      jquery_element.find('.glyph')
      .removeClass "glyphicon-#{settings.glyph}"

    apply_toggle_settings: (jquery_element, settings) ->
      jquery_element.addClass settings.class
      jquery_element.find('.glyph')
      .addClass "glyphicon glyphicon-#{settings.glyph}"
      jquery_element.find('.word')
      .text settings.word

    init_toggle_buttons: () ->
      $('[data-toggle-button]').each ->
        info = aM.get_toggle_info @
        settings = aM.toggle_settings info
        aM.apply_toggle_settings $(@), settings

    toggle: () ->
      self = @
      current_info = aM.get_toggle_info self
      new_info =
        type: current_info.type
      new_info.state = if current_info.state is 'collapse'
      then 'expand' else 'collapse'
      if new_info.state is 'collapse'
        only = aD.toggle_data[new_info.type].only
        if only
          $("#{only} [data-toggle-button].toggle-collapse").click()
        action = aD.toggle_data[new_info.type].action
        if action?
          loading_info =
            type: current_info.type
          loading_info.state = 'loading'
          aM.render_toggle_button self, current_info, loading_info
          parent = aD.toggle_data[new_info.type].parent
          parent_element = $(self).parents(parent)
          id = parent_element.attr 'value'
          filter = parent_element.data 'filter'
          if id? and filter?
            params = {}
            params[filter] = id
          skip_new = aD.toggle_data[new_info.type].skip_new?
          container = parent_element.find(".#{action}")
          $.when(aM.load_collection action, container, params, skip_new)
          .done () ->
            aM.finish_toggle self, loading_info, new_info
            aM.init_toggle_buttons()
        else
          aM.finish_toggle @, current_info, new_info
      else
        aM.finish_toggle @, current_info, new_info
    
    render_toggle_button: (element, current_info, new_info) ->
      aM.clear_toggle_settings $(element), aM.toggle_settings(current_info)
      aM.apply_toggle_settings $(element), aM.toggle_settings(new_info)

    finish_toggle: (element, current_info, new_info) ->
      aM.render_toggle_button element, current_info, new_info
      parent = aD.toggle_data[new_info.type].parent
      target = new_info.type.split '_'
      target = if target[1] then target[1] else target[0]
      $(element).parents(parent)
      .find("[data-toggle-target='#{target}']")
      .toggleClass 'hidden'
      $(element).toggleClass 'toggle-collapse'

    common_submit: () ->
      self = @
      unless $(self).hasClass 'disabled'
        route_data = $(self).data('submit').split ':'
        controller = route_data[0]
        type = aD.verbs[route_data[1]]
        parents = $(self).parents('[data-form]')
        form = parents[0]
        owner = parents[1]
        id = if type is 'POST' then '' else '/' + $(form).attr 'value'
        url = aD.server + controller + id + '.json'
        model = controller.slice 0, -1
        unless type is 'DELETE'
          params = {}
          params[model] = {}
          selector = aM.fields_selector controller
          $(form).find(selector).each ->
            params[model][$(@).data 'field'] = $(@).val()
        if type is 'POST'
          if owner?
            field = $(owner).data 'filter'
            owner_id = $(owner).attr 'value'
            params[model][field] = owner_id
        login = $(form).data('form') is 'login'
        username = if aD.username? then aD.username else params.user.username
        password = if aD.password? then aD.password else params.user.password
        $.ajax
          type: type
          url: url
          data: params
          beforeSend: (xhr) ->
            xhr.setRequestHeader "Authorization",
            "Basic " + btoa("#{username}:#{password}")
        .done (data) ->
          if login
            aD.username = username
            aD.password = password
            $.when(aM.check_username username).done () ->
              aM.render_login_info()
              aM.welcome()
          aM.render_done_state model, type, self, data
        .fail (xhr, s) ->
          aM.flash_alert xhr
    
  init: (actual = true) ->
    $('nav').on 'click', '[data-set="server"]', aM.choose_server
    $('body').on 'click', '#server-popup a', aM.set_stored_server
    $('body').on 'click', '#server-popup button', aM.set_server_manually
    $('nav').on 'click', 'a.navbar-brand', aM.welcome
    $('nav').on 'change', 'input[type="text"]', aM.data_check_username
    $('nav').on 'click', '[data-logout]', aM.logout
    $('body').on 'click', '[data-submit]', aM.common_submit
    $('body').on 'click', '#smoke-screen', aM.close_popup
    $('#work-space').on 'change',
    '[data-form] input, [data-form] textarea', aM.enable_update
    $('#work-space').on 'click', '[data-toggle-button]', aM.toggle

    location = window.location.href

    test = location.match(/Spec/)? and actual
    unless test
      delete @.test_proxy
      delete @.data_proxy
      delete @.data_set_proxy
      delete window.Application
    
    aM.init_server_popup()
    aM.set_server location

    aM.render_login_info()
    aM.welcome()

  test: () ->
    report = []
    for proxy in [ 'test_proxy', 'data_proxy', 'data_set_proxy' ]
      status = aM.security_status typeof(@[proxy]) is 'undefined'
      report.push aM.security_message proxy, status
    status = aM.security_status typeof(Application) is 'undefined'
    report.push aM.security_message 'Application class', status
    report.join ', '

  test_proxy: (private_method_name) ->
    aM[private_method_name]

  data_proxy: (key) ->
    aD[key]

  data_set_proxy: (key, value) ->
    aD[key] = value

