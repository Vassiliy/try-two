describe 'Application', ->
  beforeEach ->
    window.app = new Application()
    window.tP = app.test_proxy
    window.dP = app.data_proxy
    window.dsP = app.data_set_proxy
    setFixtures fixtures

  describe "Accessibility and testability", ->
    describe "Allows public access", ->
      public_methods = ['init', 'test_proxy', 'data_proxy', 'test']
      for public_method in public_methods
        it "to `#{public_method}()`", ->
          expect(typeof app[public_method]).toBe 'function'

    describe "Denies public access", ->
      it "to private application methods (aM)", ->
        expect(app.aM).toBeUndefined()
      it "to application data (aD)", ->
        expect(app.aD).toBeUndefined()

    describe "Allows access via test proxy", ->
      private_methods = [ 'csonify', 'data_selector', 'fields_selector',
      'security_status', 'security_message', 'load_buffered', 'init_popup',
      'open_popup', 'close_popup', 'init_server_popup', 'set_server',
      'choose_server', 'set_stored_server', 'set_server_manually',
      'render_set_server', 'flash_alert', 'render_login_info', 'welcome',
      'logout', 'data_check_username', 'check_username', 'enable_update',
      'render_done_state', 'common_template_data', 'user_template_data',
      'topic_template_data', 'post_template_data', 'render_collection_item',
      'add_create_form', 'load_collection', 'get_toggle_info',
      'toggle_settings', 'clear_toggle_settings', 'apply_toggle_settings',
      'init_toggle_buttons', 'toggle', 'render_toggle_button',
      'finish_toggle', 'common_submit' ]
      for private_method in private_methods
        it "to `aM.#{private_method}()`", ->
          expect(typeof tP(private_method)).toBe 'function'

    describe "Allows access via data proxy", ->
      it "to application data", ->
        servers = dP 'servers'
        expect(servers.local.title).toBe 'Your locally assembled deployment'

  describe "Utility methods", ->
    describe "Pretty CoffeeScriptONifier `csonify(Object)`", ->
      it "logs objects the pretty CoffeeScript way", ->
        object =
          en:
            one: 1
            two: 2
            three: 3
          jp: [ 'ichi', 'ni', 'san' ]
        log = tP('csonify')(object)
        expect(log).toContain '"en":'
        expect(log).toContain '"one":'
        expect(log).toContain '"two":'
        expect(log).toContain '"three":'
        expect(log).toContain '"jp":'
        expect(log).toContain '"ichi",'
        expect(log).toContain '"ni",'
        expect(log).toContain '"san"'

    describe "JQuery selectors factory `data_selector(key, names)`", ->
      it "creates selector with given key and names", ->
        key = 'a'
        names = [ 'b', 'c', 'd' ]
        expect(tP('data_selector')(key, names))
        .toEqual "[data-a*='b'], [data-a*='c'], [data-a*='d']"

    describe "JQuery selectors factory `fields_selector(controller)`", ->
      it "creates selector with given controller", ->
        expect(tP('fields_selector')('users'))
        .toEqual "input[data-type='users'], textarea[data-type='users']"

  describe "Console test helpers", ->
    describe 'security_status()', ->
      it "is 'secure' for true", ->
        expect(tP('security_status') true).toBe 'secure'
      it "is 'breached' for false", ->
        expect(tP('security_status') false).toBe 'breached'
    describe 'security_message()', ->
      it "is fuss-saving and adjustable", ->
        item = 'This test'
        status = 'maybe too verbose'
        expect(tP('security_message') item, status)
        .toBe 'This test is maybe too verbose'

  describe "API server address", ->
    it "is always pointing to Heroku from web deployment", ->
      tP('set_server') 'http://'
      expect(dP 'server').toContain 'herokuapp.com'

  describe "Server setup interface", ->
    it "opens at first application launch", ->
      server = localStorage.removeItem 'server'
      server = localStorage.getItem 'server'
      expect(server).toBe null
      app.init()
      server_popup = $('#server-popup')
      expect(server_popup).not.toBeHidden()
      expect(server_popup).toBeVisible()
    it "sets the manually entered server", ->
      app.init()
      $('#server-popup input').val 'http://somewhere/'
      $('#server-popup button').click()
      expect(dP 'server').toEqual 'http://somewhere/'
    it "sets stored server", ->
      app.init()
      $('#server-popup a[value="local"]').click()
      expect(dP 'server').toEqual 'http://localhost:5000/'
      $('#server-popup a[value="web"]').click()
      expect(dP 'server')
      .toEqual 'http://try-catch-vassiliy-pimkin.herokuapp.com/'

  describe "API interfacing functions", ->
    beforeEach ->
      jasmine.Ajax.install()
    afterEach ->
      jasmine.Ajax.uninstall()

    describe "Flash error notifications", ->
      beforeEach ->
        $.getJSON 'somewhere'
        .fail (xhr, s) ->
          tP('flash_alert')(xhr)
      sM =
        mock: (status, check) ->
          jasmine.Ajax.requests.mostRecent().respondWith
            status: status
          expect($('#flash .alert').html()).toContain check
      it "renders 'Not found' for 404 status", ->
        sM.mock 404, '404'
      it "renders 'Unknown error' for all other error statuses", ->
        sM.mock 499, 'Unknown error'

    describe "Username existence checker `check_username`", ->
      it 'forbids signing in for non-existent users', ->
        app.init()
        $('nav [data-field="username"]').change()
        jasmine.Ajax.requests.mostRecent().respondWith
          status: 200
          responseText: '[]'
        expect($('nav [data-submit="users:update"]')).toHaveClass 'disabled'
      it 'allows signing in for registered users', ->
        app.init()
        $('nav [data-field="username"]').change()
        jasmine.Ajax.requests.mostRecent().respondWith
          status: 200
          responseText: '[1]'
        expect($('nav [data-submit="users:update"]')).not.toHaveClass 'disabled'

    describe "Basic Authentication system", ->
      beforeEach ->
        app.init()
        $('nav [data-field="username"]').change()
        jasmine.Ajax.requests.mostRecent().respondWith
          status: 200
          responseText: '[1]'
      it 'forbids unauthorized access', ->
        $('nav [data-submit="users:update"]').click()
        jasmine.Ajax.requests.mostRecent().respondWith
          status: 401
        expect(dP 'username').toBe ''
      it 'allows authorized access', ->
        $('nav [data-field="username"]').val 'correct_username'
        $('nav [data-submit="users:update"]').click()
        jasmine.Ajax.requests.mostRecent().respondWith
          status: 200
          responseText: '[1]'
        expect(dP 'username').toBe ''

    describe "Main menu lists", ->
      sM =
        mock: (type) ->
          jasmine.Ajax.requests.mostRecent().respondWith
            status: 200
            responseText: JSON.stringify(requests[type])
        mock_guest: (type) ->
          app.init()
          $('#' + type + '-container [data-toggle-button]').click()
          sM.mock(type)
        mock_user: (type) ->
          app.init()
          dsP 'user_id', 15
          $('#' + type + '-container [data-toggle-button]').click()
          sM.mock(type)
        mock_admin: (type) ->
          app.init()
          dsP 'user_id', 1
          dsP 'admin', true
          $('#' + type + '-container [data-toggle-button]').click()
          sM.mock(type)
        read_selector:
          topics: '#topics-container .panel-heading > span'
          users: '#users-container .alert [data-toggle-button]'
        edit_selector:
          topics: '#topics-container .panel-heading > input'
          users: '#users-container .alert [data-submit]'
        check_selectors: (type, read, edit) ->
          expect($(sM.read_selector[type]).length).toEqual read
          expect($(sM.edit_selector[type]).length).toEqual edit

      describe "Topics list", ->
        it "renders read-only topics list for guests", ->
          sM.mock_guest 'topics'
          sM.check_selectors 'topics', 9, 0
        it "renders partially editable topics list for topic starters", ->
          sM.mock_user('topics')
          sM.check_selectors 'topics', 5, 5
        it "renders fully editable topics list for admins", ->
          sM.mock_admin('topics')
          sM.check_selectors 'topics', 0, 10
        it "Enables editing when topic title changed", ->
          sM.mock_admin('topics')
          topic = $('#topics-container .topics-list .panel')[0]
          $(topic).find('input').val 'New title'
          $(topic).find('input').change()
          expect($(topic).find('[data-submit="topics:update"]'))
          .not.toHaveClass 'disabled'
        it "Disables editing when topic title became empty", ->
          sM.mock_admin('topics')
          topic = $('#topics-container .topics-list .panel')[0]
          $(topic).find('input').val ''
          $(topic).find('input').change()
          expect($(topic).find('[data-submit="topics:update"]'))
          .toHaveClass 'disabled'

      describe "Users list", ->
        it "renders read-only users list for guests", ->
          sM.mock_guest 'users'
          sM.check_selectors 'users', 5, 0
        it "renders list with editable profile for user", ->
          sM.mock_user('users')
          sM.check_selectors 'users', 5, 2
        it "renders fully editable users list for admin", ->
          sM.mock_admin('users')
          sM.check_selectors 'users', 5, 10

  describe "Console proxies test", ->
    it "stays relevant in test environment", ->
      app.init()
      report = app.test()
      expect(report.match(/breached/g).length).toBe 4
    it "can emulate actual environment (kills Application class)", ->
      app.init false
      report = app.test()
      expect(report.match(/secure/g).length).toBe 4

